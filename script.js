// SOLUTION TO #3 & #4


function getCube(number){
	let cube = number ** 3
	return `The cube of ${number} is ${cube}`
}

console.log(getCube(2))

// SOLUTION TO #5 

let address = [258, `Washington Ave, NW`, `California`, 90011];

// solution to #6

 function yourAddress (currentAddress) {

 	let [number, street, state, zip] = currentAddress

 	return `I live at ${number} ${street}, ${state} ${zip}`
 }

console.log(yourAddress(address));

let animal = {
	animalName: `Lolong`,
	kind: `saltwater crocodile`,
	weight: 1075,
	feetLength: 20,
	inchesLength: 3
}

function animalInfo(yourAnimal){
	let {animalName, kind, weight, feetLength, inchesLength} = yourAnimal;

	return `${animalName} was a ${kind}. He weighed at ${weight} kgs with a measurement of ${feetLength} ft ${inchesLength} in.`
}

console.log(animalInfo(animal));

let number = [1,2,3,4,5];

number.forEach(num => console.log(num));

let sum = number.reduce(function(a,b){
	return a + b
})

console.log(sum);

class Dog {
	constructor (dogName, age, breed){
		this.dogName = dogName;
		this.dogAge = age;
		this.dogBreed = breed
	}
}

let dog1 = new Dog (`Frankie`, 5, `Miniature Dachshund`);
console.log(dog1);